﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 2 tablas
DROP DATABASE IF EXISTS b20190606;
CREATE DATABASE b20190606;

-- Seleccionar la base de datos
USE b20190606;

/*
  creando la tabla clientes
*/

CREATE TABLE socio(
  cod varchar(10),
  nombre varchar(50),
  
  PRIMARY KEY(cod)  -- creando la clave
);

CREATE TABLE telefonos(
  cod_soc varchar(10),
  tfno int,
  PRIMARY KEY(cod_soc,tfno), -- creando la clave
  CONSTRAINT fktlf_soc FOREIGN KEY(cod_soc)
  REFERENCES socio(cod) 
  );

CREATE TABLE correos(
  socio varchar(50),
  email varchar(20),
  PRIMARY KEY(socio,email), -- creando la clave
  CONSTRAINT fkcorr_soc FOREIGN KEY(socio)
  REFERENCES socio(cod)
);

CREATE TABLE peliculas(
  cod_pel varchar(10),
  titulo  varchar(50),
  PRIMARY KEY(cod_pel) -- creando la clave

  );
CREATE TABLE alquila(
  cod_soc varchar(10),
  cod_pel varchar(10),
  PRIMARY KEY(cod_soc,cod_pel), -- creando la clave
  CONSTRAINT fkalq_soc FOREIGN KEY(cod_soc)
  REFERENCES socio(cod),
  CONSTRAINT fkalq_pel FOREIGN KEY(cod_pel)
  REFERENCES peliculas(cod_pel)
  );

CREATE TABLE fecha(
  cod_soc varchar(10),
  cod_pel varchar(10),
  fecha date,
  PRIMARY KEY(cod_soc,cod_pel,fecha),
  CONSTRAINT fkfech_alq FOREIGN KEY(cod_soc,cod_pel) -- creando la clave
  REFERENCES alquila (cod_soc,cod_pel) 
  );
